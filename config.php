<?php

return [
    'production' => false,
    'baseUrl' => '',
    'title' => 'HiAndrew',
    'description' => 'Software Developer In Prince Edward Island Canada',
    'collections' => [
        'projects' => [
            'path' => 'projects'
        ],
        'blog' => [
            'path' => 'blog'
        ],
        'opensource' => [
            'path' => 'opensource'
        ],
        'arcade' => [
            'path' => 'arcade'
        ],
        'otherprojects' => [
            'path' => 'otherprojects'
        ],
    ],
];
