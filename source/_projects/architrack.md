---
extends: _layouts.project
section: projects
title: Architrack
featuredimg: '/assets/images/architrack.png'
summary: A tool for managing construction contract approvals and cost overruns
order: 2
category: software
screenshots: 3
date: 2023-2024
tags: Vue,Laravel
---
## Technologies Used
    - Laravel
    - Vue.js
    - Tailwind CSS

Architrack is a tool to help manage construction projects by consolidating contracts, communication, and approvals into a centralized location. 

### Documents
A lot of contracts and changes happen during construction projects. These changes come with cost increases, scheduling changes, and must be approved by stakeholders before they can be considered legally binding. 
Keeping track of all of this information quickly becomes overwhelming for even the most experienced teams especially since miscommunications can be very costly.

Architrack provides a simple UI to both create and track these contracts so that miscommunications no longer happen and the whole team is aware of cost overruns.

### Approvals
Every contract created during a construction project must be approved by all stakeholders before it is considered legally binding. Architrack provides a flexible approval tracking system that makes keeping track of approvals and rejections a breeze. 

### Communication
Along contracts comes long discussion chains that allow crucial details to easily get lost in email inboxes. Architrack allows for real time communication directly on the contract itself so that communication is never lost. 

Architrack also tracks when a user has viewed a document so that you are sure that everyone is aware that this contract exists.



