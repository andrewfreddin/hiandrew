---
extends: _layouts.project
section: projects
title: TinyURL
featuredimg: '/assets/images/tinyurl-logo.png'  
summary: The Worlds Oldest URL Shortening Service
order: 900
category: software
screenshots: 1
date: Spring 2022 - Present
tags: PHP,Laravel,Vue,Tensorflow
---
## Technologies Used   
    - PHP
    - Laravel 
    - Vue.js
    - Tensorflow
    - Tailwind
    - Docker
    - Mysql 

TinyURL is a URL shortening service that allows users to shorten long URLs into short, easy to remember links. We 
worked with TinyURL to develop an abuse management platform for detecting and terminating malicious links created on 
their platform.

## An AI To Detect Fraudulent Links
We built a Tensorflow based neural network that can be used to detect phishing websites within TinyURLs 
created by users. 

At a high level, the system breaks each TinyURL 
link down into a series of classifications based on patterns and heuristics derived from known phishing 
websites and 
malicious links added to TinyURL in the past. 

This data is then fed into a neural network to be analyzed by a series of hidden layers. If the neural network 
determins that the link is malicious, it will be flagged for human intervention.

#### Abuse Management Platform
We built an abuse management platform for TinyURL that allows them to detect and terminate malicious links created 
by their users. 

This platform is a Laravel based site that will automatically detect suspicious links and flag them 
for human intervention. 

The human user can then use this data to terminate malicious links and ban users who 
created them.

More details to come in the future!


