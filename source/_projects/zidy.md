---
extends: _layouts.project
section: projects
title: Zidy
featuredimg: '/assets/images/zidy.png'  
summary: A CRM tool for collecting and analyzing customer reviews about your business
order: 2
category: software
screenshots: 2
date: 2022
tags: Vue,.NET,C#
---
## Technologies Used
    - Vue.js
    - .NET Core
    - Tailwind CSS
    - Twillio (SMS)

Zidy is a CRM tool used to collect and manage customer reviews about your business. These reviews are collected and aggregated by the platform to provide you with easy access to the current customer sentiment about your business. 

I joined the Zidy team to help polish up and deliver an initial MVP version of their SMS platform when it was falling behind schedule.

### Customer Review Aggretation
Zidy works by sending messages to customers asking them to reply with a review. The application collects these reviews and analyzes/summarizes the content to provide you with the information you need to run your business.

Zidy also allows you to reply to these reviews directly to the customers device using an intuitive real time chat platform that replies using the same communication method used by the customer.
