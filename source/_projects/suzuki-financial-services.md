---
extends: _layouts.project
section: projects
title: Suzuki Financial Services
featuredimg: '/assets/images/SFS-EN.png'  
summary: An Dealer to Manufacturer financing application used in 300+ powersport dealerships across Canada
order: 900
category: software
screenshots: 6
date: Spring 2017 - Jan 2020
tags: PHP,Vue
---
## Technologies Used   
    - PHP
    - Laravel 
    - Vue.js
    - Bootstrap
    - Mysql 

Suzuki is an automotive manufacturer that sells cars, motorcycles, ATVs, and marine engines. I've worked with the Canadian division of the company on two major projects

## Suzuki Financial Services
Suzuki Financial Services is a dealer to manufacturer financing application. Dealerships use SFS to manage a financing agreement with Suzuki to float the inventory found on their showroom floors. 

This application was built using [Laravel](https://laravel.com) with a [Vue.JS](https://vuejs.org) frontend. It also has some custom integrations to interface with the legacy hardware and applications you find in companies of this size. 

#### Interest Calculations
SFS calculates and applies interest charges based on industry standard metrics. Dealers can then use this app to pay their outstanding balances directly within the application.

#### Dealership Audits
Suzuki Financial Services is also used to perform scheduled audits of dealership inventory. An auditor would go on site to each dealer and manually verify that the products listed in the application were physically present within the dealership. 

If products were missing, the auditor would flag these items within the SFS dashboard for review by Suzuki executives. 

<br />

## Suzuki Inventory Manager
 Inventory Manager was originally a windows based application that was rewritten as a web app using [Laravel](https://laravel.com). The source code for the original application was partially lost to time, meaning this project required considerable reverse engineering efforts to complete.

#### Inventory System
The application allows a dealership to view in-stock Motorcycles, ATVs, and Marine engines as well as inventory scheduled for delivery. Dealerships could also use this tool to see how long a product has been in stock to incentivize salesmen to move inventory out the door and into the hands of their customers.

#### Dealership Transfer Requests 
Dealers can use this application to search for product at other dealerships. If the product is found in-stock at another location, dealers can then initiate a "transfer request" to move this product to their location. The transfer is facilitated and fulfilled by Suzuki with the whole process managed by this application.

#### Warranty & Repair Requests
When a customer comes in for maintenance under warranty, the dealership makes a request for this work using this app. Suzuki head office then reviews the warranty request and either approves it, denies it, or requests supplemental information about the claim. 

#### Spare Parts
Dealers can order spare parts for their customers using this application. The list of products available was massive, with thousands of parts covering products built all the way back to the 1980s. 

   
       



   







