---
extends: _layouts.project
section: projects
title: Logo Garden
date: 2019-5-6
featuredimg: '/assets/images/logogarden.png'  
summary: A logo creation tool that allows you to apply your creations to promotional swag like t-shirts, mugs, etc.
order: 5 
category: software
screenshots: 2
tags: PHP,Angular,Laravel,Wordpress
---
## Technologies Used 
    - Laravel
    - Wordpress/WooCommerce
    - AngularJS, 
    - Adobe Flash
    - Varnish
    - DevOps (Vagrant, Docker, Puppet, Ansible)

Logo Garden is an art tool and e-commerce platform that allows you to build a custom logo for your business. This logo can then be applied to purchasable products such as coffee mugs, t-shirts, and other promotional swag.

My job consisted of porting the original logo tool, built in [Adobe Flash](https://en.wikipedia.org/wiki/Adobe_Flash), to Javascript using [AngularJS](https://angularjs.org/). I also helped rewrite the legacy [PHP](https://www.php.net) e-commerce platform over to a new site built using [Laravel](https://laravel.com/).
