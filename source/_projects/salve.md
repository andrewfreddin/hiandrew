---
extends: _layouts.project
section: projects
title: Salve
featuredimg: '/assets/images/salve.jpg'  
summary: A Seed To Sale Management Platform For Retail Cannabis Dispensaries 
order: 8
category: software
screenshots: 3
date: Summer 2021 (Ongoing)
tags: PHP,Vue,robotics
---
## Technologies Used
    - PHP
    - Laravel 
    - Vue.js
    - Tailwind
    - Mysql      

Salve is a software platform used manage retail cannabis dispensaries. 

I had two principal responsibilities at Salve

## A Communications Platform Using the WWKS2 Protocol
During my time at Salve, I Architected and built a communications platform between our [Laravel](https://laravel.com/) based cloud platform and a physical robot used to make and dispense prescription drugs for pharmacies. I achieved this by implementing the WWKS2 robotics protocol as a PHP library. PHP is a dynamic language and inherently incompatible with hardware that expects exact types and data structures so there was a lot redundancy, runtime checks, and automated testing required to make this platform reliable.

## A Scaleable E-commerce platform For The Cannabis Industry
Salve sells a turnkey retail/online commerce solution to cannabis dispensaries and for reasons ranging from technical, to legal, existing solutions (example Woocommerce, Shopify) were not compatible with this goal. I designed and built a scalable platform, similar to existing tools, but tailored to the exact needs of Cannabis dispensary owners. 

I built this platform on top of [Nuxt.js](https://nuxtjs.org/) (Vue) with a complicated Vuex based state management system to perform the heavy lifting. As of the time of writing, this platform is in use by one very happy customer (www.theflowery.co) and hopefully soon will have been used to simplify the lives of other cannabis dispensary owners, too.
