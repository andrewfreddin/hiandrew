---
extends: _layouts.project
section: projects
title: iWave
featuredimg: '/assets/images/iwave.png'  
summary: A tool to simplify fundraising efforts for non profits
order: 850
category: software
screenshots: 1
tags: PHP,Vue,Symfony
---

## Technologies Used
    - PHP
    - Symfony (3 & 4)
    - API Platform 
    - Ext JS
    - Vue.js
    - Vuex
    - Vuetify 
    - Docker


iWave is a tool that simplifies the fundraising process for nonprofits & educational institutions by helping to connect them to donors that have the interest and capacity to further their cause. By using their software, you’ll be able to quickly scan for donors with the greatest propensity to give along with who is most likely to support your capital campaign.

iWave was originally built many years ago using [Symfony 3](https://symfony.com/) and a frontend framework called [EXT JS](https://www.sencha.com/products/extjs/). After finding some success and starting to feel the limitations of the original application, management decided it was time to rebuild. 

## Maintaing A Legacy Symfony Codebase
I was brought in as a temporary onboarded employee to maintain their existing app while their internal development team (approximately 7-10 people) rebuilt their codebase using API Platform ([Symfony 4](https://symfony.com/)) and [Vue.js](https://vuejs.org/). This involved fixing issues reported by iWave's users/clients and occasionally back porting features from the new codebase to the old one. 

## Building Features For the New Vue.js Frontend
 When I didn't have any maintenance work to do, I would assist the front end team by tasking myself with completing features and tickets from their project tracker (Jira). I built a ton of features in both the [Vue.js](https://vuejs.org/) front end and also in the [Vuex](https://vuex.vuejs.org/) based state management system that did the heavy lifting behind the scenes. By the end of my time at iWave I was essentially a full time front end dev even if that wasn't my official role. 

## Assisting Junior Developers
iWave had a couple of junior developers who were eager to contribute, but were underwater from a technical standpoint. I took the initiative to keep an eye on the less experienced members of their team and when I noticed them struggling, would take some time out of my day to help them. 

Afterwards, If they were still having issues, I would come back and show them the solution I would use and we would implement it together. 

Over time, these team members started submitting more tickets with less assistance and I like to think I was a part of making that happen.