---
extends: _layouts.project
section: projects
title: Justice Knowledge Network
featuredimg: '/assets/images/rcmp.png'  
summary: A booking system for the RCMP barracks and housing programs
order: 7
category: software
screenshots: 0
tags: PHP,Angular2,Laravel
---
## Technologies Used
    - PHP
    - Laravel 
    - Ilias
    - Angular 2 
    - Typescript
    - Mysql
    - SOAP (Simple Object Access Protocol)

The RCMP is a nation wide canadian police service famous for their red jackets and horse mounted officers. I helped to build an application for managing the barracks and housing of cadets and officers while stationed on training bases.

My job consisted of helping the in-house team build an appplication using [Laravel](https://laravel.com/) and [Angular 2](https://angular.io/) with a custom built library allowing communication over the [SOAP](https://en.wikipedia.org/wiki/SOAP) protocol. 

I also did hands-on training with a few (at the time)JR employees who now hold Senior roles in the company.

Unfortunately I don't have any pictures of this app but I assure you it's really nice.
      
    




