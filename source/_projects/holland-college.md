---
extends: _layouts.project
section: projects
title: Holland College
featuredimg: '/assets/images/hollandcollege.png'  
summary: The homepage for Holland College, a trade school located in various locations across Prince Edward Island
order: 2
category: software
screenshots: 2
date: 2014-2015
tags: PHP,Wordpress
---
## Technologies Used
    - PHP
    - Javascript
    - JQuery
    - Wordpress
    - Windows IIS
    - Microsoft SCCM

Holland College is a relatively large trade school located in Prince Edward Island, Canada. 
The website is a hybrid between a Wordpress blog and a "legacy PHP" website that was originally built using PHP 3 (!) 

My job here consisted of porting a relatively complicated [PHP 3](https://www.php.net) app to the Wordpress CMS platform. 

This consisted of porting/rewriting a bunch of small applications designed for a single service to a single monolith platform and updating existing code to the highest available PHP version at the time (5.6). 
