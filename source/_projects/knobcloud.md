---
extends: _layouts.project
section: projects
title: KnobCloud
featuredimg: '/assets/images/knobcloud-logo.svg'  
summary: An Online Marketplace For Buying And Selling Music Plugins
order: 100
category: software
screenshots: 1
date: Fall 2021 - Present
tags: PHP,Laravel,Vue
---
## Technologies Used   
    - PHP
    - Laravel 
    - Vue.js
    - Tailwind
    - Docker
    - Mysql 

KnobCloud is an online music marketplace for buying and selling music plugins. It is similar to Craiglist or Kijiji 
in that it allows consumers to buy and sell previously owned products, but since these goods are all-digital, 
transactions can be performed between creators all across the world. 

We built and maintain Knobcloud as well as help the owners accelerate growth through online marketing and outreach.

More details to come in the future!




