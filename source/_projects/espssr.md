---
extends: _layouts.project
section: projects
title: ESP SSR
featuredimg: '/assets/images/ssr.png'  
summary: ESP is a bespoke ERP for planning and procuring inventory for new and existing hospitals
order: 2
category: software
screenshots: 2
date: 2022-2023
tags: Vue,Laravel
---
## Technologies Used
    - Laravel
    - Legacy PHP
    - Vue.js

ESP is a bespoke ERP for planning and procuring inventory for new and existing hospitals. 

I worked with a team at [ByThePixel](https://bythepixel.com/) to rewrite an existing legacy php application into a modern SPA with a Vue.js front end client and Laravel backend. 

