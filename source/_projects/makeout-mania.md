---
extends: _layouts.project
section: projects
title: Bight Games
featuredimg: '/assets/images/makeout-mania.jpg'  
summary: A video game company that made games for mobile phones 
order: 0
category: software
screenshots: 4
date: Spring 2020
tags: games
---
## Technologies Used 
    - C++
    - Java (J2ME)
    - Brew
    - OpenGL    

Bight Games was a video game company that made games for mobile phones. Their bread and butter was taking games 
that were breakout hits on the iPhone, and porting them to "dumb phones" running on the BREW and J2ME platforms.

The main project I worked on was a game called "Makeout Mania" that we ported from the "iPhone 3G" to over 100 
different phones. 

The code was written in C++ with a middleware that would re-compile to Java Bytecode for J2ME based devices. 
Some of these phones were very weak with ram in the 20s of KB making this a very challenging project. 

Bight Games was [purchased by Electronic Arts](https://www.ea.com/news/ea-acquires-bight-games) in 2011. A testament to the quality work we achieved as a team.  
