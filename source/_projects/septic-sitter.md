---
extends: _layouts.project
section: projects
title: Septic Sitter
featuredimg: '/assets/images/septicdark.png'  
summary: An IOT device and cloud based management software for septic systems
order: 9
category: software
screenshots: 5
date: Spring 2017
tags: PHP,Vue,IOT
---
## Technologies Used
    - PHP
    - Laravel 
    - Vanilla Javascript
    - Vue.js
    - Bootstrap
    - Mysql (Cloud) & Sqlite (IOT devices)
    - Docker
    - IOT Device built on the UDOO platform 

Septic Sitter is a cloud based management platform for residential septic systems. 
The software is used by commercial plumbing companies to monitor residential septic systems remotely. If issues such as freezing or flooding are detected, the plumbing company and homeowner are notified immediately via either e-mail or SMS message. 

I was brought on to perform a full rewrite of Septic Sitter's legacy [PHP](https://www.php.org) codebase to a new application built on the [Laravel](https://laravel.com/) framework. This consisted of a complete rewrite of the cloud application, the on-site management application, and the client facing web frontend. 

I finished the PHP rewrite well ahead of schedule, so with the remaining time, I also performed a full rewrite of 
the front end admin dashboard using [Vue.js](https://www.vuejs.org)
