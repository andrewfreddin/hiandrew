---
extends: _layouts.project
section: projects
title: Adamson Flooring
featuredimg: '/assets/images/flooring.png'  
summary: A bespoke application for managing inventory and scheduling for a flooring company
order: 2
category: software
screenshots: 3
date: 2022-2023
tags: Vue,Laravel
---
## Technologies Used
    - Laravel
    - Vue.js
    - Tailwind CSS

Adamson Flooring is a bespoke application for managing inventory and scheduling for a flooring company.

## Inventory
The application is used to manage inventory, both existing and incoming, for upcoming jobs. When a job is added, the application will automatically calculate how much flooring is needed for the job based on the square footage. Flooring is sold in boxes with varying quantities, so these calculations are then used to determine how many boxes are required for a job.

## Job Planning & Scheduling
The application is also used to to schedule jobs for internal teams and external vendors. The scheduling system tracks each employee and vendor individually to ensure that teams do not end up under utilized or double booked. 

## Sage Accounting Integration
The application will automatically sync invoices and vendor bills with sage accounting allowing for seamless collaboration between the operations and accounting teams.

