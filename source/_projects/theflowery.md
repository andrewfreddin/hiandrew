---
extends: _layouts.project
section: projects
title: The Flowery
featuredimg: '/assets/images/theflowery.png'  
summary: A Florida based E-Commerce site that sells and delivers medical cannabis
order: 800
category: software
screenshots: 8
date: Fall 2021
tags: vue,vuex,ecommerce 
---
## Technologies Used 
    - Javascript
    - Nuxt
    - Vue.js 
    - Vuex
    - Tailwind CSS

The Flowery is an online e-commerce site for a chain of retail medical cannabis dispensaries in Florida USA. The site was built almost entirely by me using [NuxtJS](https://nuxtjs.org/) with [Vuex](https://vuex.vuejs.org/) for state management. 

## Delivery System
The Flowery provides home delivery to their customers kind of like what Amazon provides, but for medical cannabis. When a user provides a new address, or selects an address already on file, the e-commerce site will determine the closest delivery warehouse. This warehouse is then used to provide stock information, pricing, and any other region specific data for that location. 

This process is completely seemless and instantaneous to the medical patient.    

## Shopping Cart
A custom shopping cart was built for The Flowery that reacts to products added to cart by medical patients. 

When new products are added to the patient's shopping cart, the entire site will update with a new total price, as well as any taxes and fees that come along with that product. This all happens seamlessly without affecting the end user experience for the user.

This functionality is accompanied by a simple to use API making it quick and easy for the developer to add or modify cart data anywhere on the site.

## Shop Filters
The Flowery has a powerful querying and sorting system for displaying products available to purchase to the end user. 

Products can be filtered and sorted by any available data metric available (examples in use include brands, strains, pricing, potency) allowing the end user to only view products they are interested in purchasing. 

Products can be filtered on either the back end when querying, or on the front end, after receiving data from the back end server. 

This functionality is exposed via a simple to use API that makes it very easy to use and extend by developers building and maintaining new features.

## Bank Accounts/ACH
Due to restrictions on the use of debit/credit cards in the medical cannabis industry, The Flowery uses Aeropay to allow purchases directly from a user's bank account. 

This functionality is exposed via a simple to use, modular API so that Bank accounts can be accessed and modified anywhere on any page of the site. 
