@extends('_layouts.main')

@section('body')
    <div class="px-4 py-5">
        <h1>Blog</h1>
        <div class="container-fluid">
            <ul>

                @foreach($blog->where("published", true)->sortByDesc("order") as $project)
                    <li>
                    <div class="col-lg-4 col-md-6 grid-item mb-4">
                        <div class="listing-item max-w-xs max-h-28">
                            <div class="position-relative">
                                <a class="reset-anchor d-block listing-img-holder" href="{{ $project->getUrl() }}">
                                    <p class="mb-0 text-primary small d-flex align-items-center listing-btn"><span>Look inside</span>
                                        <svg class="svg-icon text-primary svg-icon-sm ml-2">
                                            <use xlink:href="#arrow-right-1"></use>
                                        </svg>
                                    </p>
                                </a>
                            </div>
                            <div class="py-3">
                                <a class="reset-anchor" href="{{ $project->getUrl() }}">
                                    <h2 class="h5 listing-item-heading">{{ $project->title }}</h2>
                                </a>
                                <p class="text-small mb-0 listing-item-description">{{ $project->summary }}</p>
                            </div>
                        </div>
                    </div>
                    </li>
                @endforeach
            </ul>
        </div>
    </div>
@endsection
