<footer class="text-muted" style="background: #0d0d0d">
    <div class="container-fluid py-5">
        <div class="row">
            <div class="col-lg-10 mx-auto">
                <div class="row">
                    <div class="col-lg-4 mb-4 mb-lg-0">
                        <h2 class="h4 text-white mb-4">About me</h2>
                        <p class="text-small">I’m a software developer with over 15 years of experience and a proven
                            track record of success.</p>
                        <p class="text-small">If you're in need of an expert in PHP, please feel free to contact me!</p>
                        <ul class="list-unstyled text-small mb-0 text-white">
                            <li class="mb-1"><a class="reset-anchor" href="#"> <i
                                            class="fas fa-globe-americas text-muted mr-2 fa-fw"></i>224 Queen St,
                                    Charlottetown PE</a></li>
                            <!-- <li class="mb-1"><a class="reset-anchor" href="#"> <i class="fas fa-mobile text-muted mr-2 fa-fw"></i>123-456-789</a></li> -->
                            <li class="mb-1"><a class="reset-anchor" href="#"> <i
                                            class="fas fa-envelope text-muted mr-2 fa-fw"></i>andrew@bytepath.ca</a></li>
                        </ul>
                    </div>
                    <div class="col-lg-4 mb-4 mb-lg-0 d-xs-none d-none d-md-block">
{{--                        <h2 class="h4 text-white mb-4">Follow me</h2>--}}
{{--                        <ul class="list-inline">--}}
{{--                            <div class="row text-white text-small">--}}
{{--                                <div class="col-6">--}}
{{--                                    <ul class="list-unstyled">--}}
{{--                                        <li><a class="reset-anchor" href="https://github.com/andrewfreddin"><i--}}
{{--                                                        class="fab fa-github mr-2 mb-2 fa-fw"></i>Github</a></li>--}}
{{--                                        <li><a class="reset-anchor" href="https://bitbucket.org/andrewfreddin"><i--}}
{{--                                                        class="fab fa-bitbucket mr-2 mb-2 fa-fw"></i>Bitbucket</a></li>--}}
{{--                                    </ul>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </ul>--}}
                    </div>
                    <div class="col-lg-4 mb-4 mb-lg-0 d-md-none">
                        <h2 class="h4 text-white mb-4"><a class="reset-anchor" href="/about">About</a></h2>
                    </div>

                    <div class="col-lg-4 mb-4 mb-lg-0 d-md-none">
                        <h2 class="h4 text-white mb-4"><a class="reset-anchor" href="/contact">Contact</a></h2>
                    </div>

                    <div class="col-lg-4 mb-4 mb-lg-0">
                        <h2 class="h4 text-white mb-4"><a class="reset-anchor" href="/blog">Blog</a></h2>
                        <ul class="list-unstyled mb-0 d-xs-none d-none d-md-block">
                            @foreach($blog->where("published", true)->sortByDesc("order")->slice(0,3) as $post)
                            <li>
                                <a class="reset-anchor" href="{{ $post->getUrl() }}">
                                    <div class="media">
                                        <img class="rounded-circle" src="img/news-1.jpg" alt="" width="50">
                                        <div class="ml-3 media-body">
                                            <p class="text-white mb-0">{{ $post->title }} </p>
                                            <p class="small mb-1"></p>
                                            <p class="text-gray text-small"></p>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="bg-dark py-4">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-10 mx-auto">
                    <div class="row text-white">
                        <div class="col-md-6 text-center text-md-left">
                            <p class="text-small mb-3 mb-md-0"><span class="text-muted">&copy; All rights reserved - HiAndrew.</span>
                            </p>
                        </div>
                        <div class="col-md-6 text-center text-md-right">
                            <p class="text-small mb-0">
                                <span class="text-muted">Thanks for visiting. Stop in again some time!</span>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
