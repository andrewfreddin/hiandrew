<!DOCTYPE html>
<html lang="{{ $page->language ?? 'en' }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="canonical" href="{{ $page->getUrl() }}">
        <meta name="description" content="{{ $page->description }}">
        <title>{{ $page->title }}</title>

        <meta name="robots" content="all,follow">
        <!-- Bootstrap CSS-->
        <link rel="stylesheet" href="/assets/theme/vendor/bootstrap/css/bootstrap.min.css">
        <!-- Lightbox CSS-->
        <link rel="stylesheet" href="/assets/theme/vendor/lightbox2/css/lightbox.min.css">
        <!-- Google fonts-->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,700&amp;display=swap">
        <!-- theme stylesheet-->
        <link rel="stylesheet" href="/assets/theme/css/style.default.css" id="theme-stylesheet">
        <!-- Custom stylesheet - for your changes-->
        <link rel="stylesheet" href="/assets/theme/css/custom.css">
        <!-- Favicon-->
        <link rel="shortcut icon" href="/assets/theme/img/favicon.png">
        <!-- Tweaks for older IEs--><!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->

    </head>
    <!-- Google tag (gtag.js) -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-YJ0HSB4YTE"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'G-YJ0HSB4YTE');
    </script>
    
    <body>
        @include("_layouts/_sidebar")

        <div class="page-holder">
            <div class="px-4 d-block d-lg-none">
                <!-- navbar-->
                <header class="header">
                    <nav class="navbar navbar-expand-lg navbar-light px-0">
                        <button class="navbar-toggler navbar-toggler-right text-sm text-uppercase" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                            <svg class="svg-icon svg-icon-heavy svg-icon-sm text-dark">
                                <use xlink:href="#menu-hamburger-1"> </use>
                            </svg>
                        </button><a class="navbar-brand" href="index.html"><img src="img/boxes.svg" alt="" width="50"></a>
                    </nav>
                </header>
            </div>
        @yield('body')
        @include("_layouts/_footer")
        </div>

        <!-- JavaScript files-->
        <script src="/assets/theme/vendor/jquery/jquery.min.js"></script>
        <script src="/assets/theme/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
        <script src="/assets/theme/vendor/masonry-layout/masonry.pkgd.min.js"></script>
        <script src="/assets/theme/vendor/imagesloaded/imagesloaded.pkgd.min.js"></script>
        <script src="/assets/theme/vendor/lightbox2/js/lightbox.min.js"></script>
        <script src="/assets/theme/js/front.js"></script>
        <link rel="stylesheet" href="https://unpkg.com/@highlightjs/cdn-assets@11.6.0/styles/default.min.css">
        <script src="https://unpkg.com/@highlightjs/cdn-assets@11.6.0/highlight.min.js"></script>

        <script>
            function docReady(fn) {
                // see if DOM is already available
                if (document.readyState === "complete" || document.readyState === "interactive") {
                    // call on next available tick
                    setTimeout(fn, 1);
                } else {
                    document.addEventListener("DOMContentLoaded", fn);
                }
            }

            docReady(function() {
                hljs.highlightAll();
            });
        </script>

        <!-- FontAwesome CSS - loading as last, so it doesn't block rendering-->
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css"
              integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">


    </body>
</html>
