<div class="sidebar">
    <div class="sidebar-inner d-flex flex-column">
        <div class="px-2 pt-5">
        <div class="grid-item pb-2">
            <div class="max-w-xs max-h-28">
                <div class="position-relative">
                    <img src="/assets/theme/img/boxes.svg" alt="" width="">
                </div>
                <div class="py-3">
                        <h2 class="h5 text-black" style="color: rgb(18, 14, 24);">Hi, I'm Andrew.<br />I write software and fix arcade machines!</h2>
                </div>
            </div>
        </div>
        </div>

        <div class="sidebar-menu-holder flex-grow-1">
        @php
        $curPath = explode("/",$page->getPath());
        $curCol = "";

        if(sizeof($curPath) > 1)
        {
            $curCol = $curPath[1];
        }
        @endphp

            <ul class="sidebar-menu list-unstyled">
                <li class="mb-2 pb-1">
                    <!-- Link--><a class="sidebar-link h6 text-uppercase letter-spacing-2 font-weight-bold
                    @if($curCol == "" || $curCol == "projects")
                            active
                    @endif
                    text-small"
                                   href="/">Work</a>
                </li>

                <li class="mb-2 pb-1">
                    <!-- Link--><a class="sidebar-link h6 text-uppercase letter-spacing-2 font-weight-bold
                    @if($curCol === "opensource")
                            active
                    @endif
                    text-small"
                                   href="/opensource">Open Source</a>
                </li>

                <li class="mb-2 pb-1">
                    <!-- Link--><a class="sidebar-link h6 text-uppercase letter-spacing-2 font-weight-bold
                    @if($curCol === "arcade")
                            active
                    @endif
                    text-small"
                                   href="/arcade">Arcade Machines</a>
                </li>

                <li class="mb-2 pb-1">
                    <!-- Link--><a class="sidebar-link h6 text-uppercase letter-spacing-2 font-weight-bold
                    @if($curCol === "otherprojects")
                            active
                    @endif
                    text-small"
                                   href="/otherprojects">Other Projects</a>
                </li>

                <li class="mb-2 pb-1">
                    <!-- Link--><a class="sidebar-link h6 text-uppercase letter-spacing-2 font-weight-bold
                    @if($curCol === "blog")
                            active
                    @endif
                    text-small"
                                   href="/blog">Tech Blog</a>
                </li>

{{--                <li class="mb-2 pb-1">--}}
{{--                    <!-- Link--><a--}}
{{--                            class="sidebar-link h6 text-uppercase letter-spacing-2 font-weight-bold text-small--}}
{{--                    @if($curCol == "about")--}}
{{--                                    active--}}
{{--                    @endif--}}
{{--"--}}
{{--                            href="/about">About</a>--}}
{{--                </li>--}}

                <li class="mb-2 pb-1">
                    <!-- Link--><a
                            class="sidebar-link h6 text-uppercase letter-spacing-2 font-weight-bold text-small
                    @if($curCol == "contact")
                                    active
                    @endif
"
                            href="/contact">Contact</a>
                </li>
            </ul>
        </div>
    </div>
</div>
