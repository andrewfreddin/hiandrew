@extends("_layouts/main")

@section('body')
    <div class="px-4 py-5">
        <div class="container-fluid">
            <div class="row mb-5">

                <div class="position-sticky">
                    <h2>{{ $page->title }}</h2>



                    <p class="text-muted">{!! $page !!}</p>

                </div>
            </div>
            <h2 class="h3 mb-4">Similar Blog Posts</h2>
            <div class="row">
                @foreach($blog->random(3) as $project)
                    <div class="col-lg-4 mb-4">
                        <div class="listing-item pl-0">
                            <div class="position-relative"><a class="reset-anchor d-block listing-img-holder"
                                                              href="{{ $project->getUrl() }}">
                                    <p class="mb-0 text-primary small d-flex align-items-center listing-btn"><span>Look inside</span>
                                        <svg class="svg-icon text-primary svg-icon-sm ml-2">
                                            <use xlink:href="#arrow-right-1"></use>
                                        </svg>
                                    </p>
                                </a></div>
                            <div class="py-3"><a class="reset-anchor" href="{{ $project->getUrl() }}">
                                    <h2 class="h5 listing-item-heading">{{ $project->title }}</h2></a>
                                <p class="text-small mb-0 listing-item-description">{{ $project->summary }}</p>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>

        <script>
            console.log('cat');
        </script>
@endsection
