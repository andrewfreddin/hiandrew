@extends("_layouts/main")

@section('body')
    <div class="px-4 py-5">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-7">

                    <h1>{{ $page->title }}</h1>
                    <p class="text-muted mb-5">{!! $page->summary !!}</p>

                </div>
            </div>
            <div class="row mb-5">
                <div class="col-lg-7">
                    @for($i=1;$i<=$page->screenshots;$i++)
                    <img class="img-fluid mb-4"
                         src="/assets/images/screenshots/{{ $page->getFilename() }}/{{$i}}.png"
                     alt="">
                    @endfor
                </div>
                <div class="col-lg-5 position-sticky">
                    <p class="text-muted">{!! $page !!}</p>
                </div>
            </div>
            <h2 class="h3 mb-4">Similar projects</h2>
            <div class="row">
                @foreach($projects->random(3) as $project)
                    <div class="col-lg-4 mb-4">
                        <div class="listing-item pl-0">
                            <div class="position-relative"><a class="reset-anchor d-block listing-img-holder"
                                                              href="{{ $project->getUrl() }}"><img
                                            class="img-fluid rounded-lg" src="{{ $project->featuredimg }}" alt="">
                                    <p class="mb-0 text-primary small d-flex align-items-center listing-btn"><span>Look inside</span>
                                        <svg class="svg-icon text-primary svg-icon-sm ml-2">
                                            <use xlink:href="#arrow-right-1"></use>
                                        </svg>
                                    </p>
                                </a></div>
                            <div class="py-3"><a class="reset-anchor" href="{{ $project->getUrl() }}">
                                    <h2 class="h5 listing-item-heading">{{ $project->title }}</h2></a>
                                <p class="text-small mb-0 listing-item-description">{{ $project->summary }}</p>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
@endsection
