---
extends: _layouts.project
section: projects
title: Bytepath
featuredimg: '/assets/images/bytepath_sm.png'  
summary: A Javascript based SVG graphics & animation framework 
order: 3
category: software
screenshots: 0
date: Spring 2020
tags: Vue,SVG,Animation 
---
https://pensive-newton-4747be.netlify.app/
