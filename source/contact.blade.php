@extends('_layouts.main')

@section('body')
    <div class="">
        <div class="px-4 py-5">
            <div class="container-fluid">
                <div class="p-5 mb-5 bg-light">
                    <h2>Contact</h2>
                    <form name="contact" method="POST" netlify>
                        <div class="row">
                            <div class="col-sm-12 col-md-6 pt-4">
                                <label for="txtname">Name</label>
                                <input type="text" id="txtname" name="name" class="form-control" placeholder="Name">
                            </div>
                            <div class="col-sm-12 col-md-6 pt-4">
                                <label for="txtemail">Email</label>
                                <input type="email" id="txtemail" name="email" class="form-control" placeholder="Email">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 pt-4">
                                <label for="txtmessage">Message</label>
                                <textarea style="resize: none;" class="form-control" id="txtmessage" name="message" rows="3">
                                </textarea>
                            </div>
                        </div>

                        <div class="row pt-3">
                            <div class="col-sm-12 col-md-4 pt-4">
                                <button type="submit" class="btn btn-primary btn-block">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    </div>
@endsection
