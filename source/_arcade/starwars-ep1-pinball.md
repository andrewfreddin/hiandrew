---
extends: _layouts.arcade
section: projects
title: Starwars Episode 1 Pinball
featuredimg: '/assets/images/ep1pinball.jpg'  
summary: Refurbishing a Starwars Episode 1 Pinball Machine!
order: 1
category: software
screenshots: 0
date: Fall 2022
tags: arcade,pinball,electronics
published: true
---
Starwars episode 1 pinball is a pretty cool machine. It's built on the 
[Pinball 2000](https://en.wikipedia.org/wiki/Pinball_2000) platform and actually projects real holograms on to the 
playfield that are integrated into the game itself. This was achieved by building an actual CRT monitor into the cabinet
that points downwards toward the playfield. The glass on the pinball machine has a special coating of some 
sort that changes the angle of reflection from the video from this monitor towards the eyes of the player. The end 
result is a pretty convincing hologram that was very impressive for the time.

Pinball 2000 was expected to be the next big thing in pinball, but unfortunately never seemed to catch on. Only two 
games ever reached production: 

- [Revenge From Mars](https://www.youtube.com/watch?v=H3Rp3NZkin4) - A Sequel to "Attack from Mars" (one of my 
favourite pinball machines) 

- [Starwars Episode 1](https://www.youtube.com/watch?v=yw7ZGbULRAw) - A pinball machine released to coincide with 
  the movie of the same name. 


Both of these machines are essentially the same game. They both share the same playfield and internals, and 
essentually just have different artwork and sound. 

### Getting The Machine Out Of Storage

I was lucky enough to find a Starwars Episode 1 machine for a
very affordable price a few years ago and have been slowly restoring it to it's former glory.

I actually had the machine up and running. It was playable, but had some minor issues such as a broken ramp and some
flippers that needed maintanance.

Then [Hurricane Fiona](https://www.cbc.ca/news/canada/prince-edward-island/pei-hurricane-fiona-damage-photos-1.6593377)
happened and caused major damage to my workshop. Luckily no machines were damaged, but in order to repair the building
all machines had to be moved to storage as quickly as possible. The star wars refurbish was essentally back to square
one.

### The Machine 

 This all happened roughly a year ago and the machine has finally been moved 
back to the shop! 
 
It looks a lot more like a pile of parts than a working machine
at the moment!

![starwars-pinball](/assets/images/arcade/starwars-pinball-refurbish/1.webp)

Once the parts were cleaned up and installed, it started to look a lot more like a pinball machine. However, when I 
turned it on, the marquee lit up, but the game did not boot. 

![starwars-pinball](/assets/images/arcade/starwars-pinball-refurbish/3.webp)




### Performing Brain Surgery

The pinball 2000 platform, like most modern pinball machines, is actually just a PC that interfaces with the playfield
using a couple of "off the shelf" (for the time) cables. The computer is mounted above the CRT in the top of the 
cabinet, and has a very loud, annoying fan, presumably due to the heat that would come from the monitor.

![starwars-pinball](/assets/images/arcade/starwars-pinball-refurbish/6.webp)

Since this machine is powered by a normal PC, it has a normal PC power supply that accepts a 3 pin universal 
power cable. 
These cables are typically plugged into the wall, but since we don't want to have to plug in multiple cables to
turn on the machine, the power cable is converted into a 3 pin connector that is pretty common in the arcade world.
These cables supply 120v AC so make sure the machine is turned off and unplugged before handling these. 

![starwars-pinball](/assets/images/arcade/starwars-pinball-refurbish/14.webp)
![starwars-pinball](/assets/images/arcade/starwars-pinball-refurbish/15.webp)


The PC communicates with the playfield using a normal, off the shelf parallel cable (sometimes called printer cables)
and a serial cable. These cables run from the PC to ports directly behind the screen 

![starwars-pinball](/assets/images/arcade/starwars-pinball-refurbish/7.webp)
![starwars-pinball](/assets/images/arcade/starwars-pinball-refurbish/8.webp)

It turned out that the reason the machine wouldn't boot up was that these connections were loose and not making a proper
connection between the PC and the playfield. Once these were reconnected, the machine booted up. 

### LCD Replacement

You might have noticed that there is an LCD monitor sitting on the cabinet. The original CRT monitor is currently
suffering from something called
[vertical collapse](https://www.arcaderepairtips.com/2011/04/07/repairing-monitor-collapse-issues/#:~:text=Vertical%20collapse%20usually%20occurs%20when,(or%20Integrated%20Circuit)%20chip)

A CRT monitor is essentially a gun that fires electrons at a screen that is covered in a phosphor coating. The monitor
typically shoots these electrons from left to right, before moving downwards to the next line.

A vertical collapse happens when something is preventing the gun from aiming upwards
and downwards, which results in a single horizontal line of seemingly random colours being drawn. These colors
are not random, it's just drawing what should be an entire screen on a single line.

This is a common issue that isn't all that hard to fix, but since I do not have a hot air rework station to replace
a chip on the CRT motherboard, the easiest solution is to replace the CRT with an LCD.

You can accomplish this by using an off the shelf 
[RGV/CGA/EGA/YUV to VGA converter board](https://www.aliexpress.com/item/4000042626038.html)

(Note that this is not the exact seller I used but the board is the same)

These boards used to be easy to find, but in the past few years have become popular for upscaling retro game consoles
making them a bit more rare. Luckily I bought a bunch of them years ago before that happened.

Wiring up these boards is mostly straightforward, but pinball 2000 has a quirk where because the player actually 
views a *reflection* of the screen, the game actually flips the screen horizontally. So if you just wire one of 
these directly to a monitor, the holograms, (and more importantly the text) projected on to the playfield will
be in backwards. You can fix this using another off the shelf IC that you wire between the screen and the game.  

### Fixing The Coin Door

Although the game would now boot up with lights and sounds, the game would not start when you pressed the start button.
By looking at the screen, we can see that it displays a warning that the coin door is open (it's not). 

Note the horizontally flipped text. This is due to the quirk mentioned above.

![starwars-pinball](/assets/images/arcade/starwars-pinball-refurbish/9.webp)

Pinball machines typically have a method of determining when the coin door is open and closed. When the door is open,
the game goes into a "service mode" where the power to the game itself is turned off so that an operator can safely
diagnose issues without getting shocked. 

The coin door is considered "closed" when the white switch in the picture below is pressed. The coin door has a piece of
metal attached to it that should press this switch when closed. This metal latch must have been damaged during the move
and just needed to be slightly bent farther towards the switch. Afterwards, the game would now play when you closed
the coin door. 

![starwars-pinball](/assets/images/arcade/starwars-pinball-refurbish/10.webp)

## A Broken Button
Although the game now boots up and is partially playable, I noticed a problem during my initial test play -- the button
on the right side of the cabinet was broken off during the move. I was able to stick a mini screwdriver in the hole 
and confirm that the flipper actuates when you press the button, but it's not exactly an ergonomic way to play. 

Fixing this button requires disassembling the playfield so it looks like this is as far as I am going to get for now. 

When on the topic of buttons, note the secondary button that surrounds the typical flipper button you see on most 
machines. This is a secondary button that is only found on "Pinball 2000" cabinets. You use it during bonus rounds
to fire lasers and other projectiles at the holograms flying around the playfield.  

These buttons still seem to work correctly which is lucky because finding a replacement would have been a pain. 

![starwars-pinball](/assets/images/arcade/starwars-pinball-refurbish/11.webp)


### Next Steps

The game now works again which is good, but still requires a lot of maintenance. In addition to the broken button, the
playfield has a broken ramp that needs to be replaced, and a couple of the playfield bumpers appear to be broken. 

But progress has been made, and thats the first step towards success!
