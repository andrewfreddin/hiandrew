---
extends: _layouts.arcade
section: projects
title: Replacing A CRT Monitor with an LCD panel
featuredimg: '/assets/images/ep1pinball.jpg'  
summary: I temporarily replaced the CRT in my Star Wars Pinball machine with an LCD panel
order: 1
category: software
screenshots: 0
date: Nov 5 2023
tags: arcade,pinball,starwars,electronics
published: false
---

### Vertical Collapse
Star wars episode one pinball has a pretty cool feature where it projects holograms onto the playfield. This is achieved
using a CRT monitor built into the top of the cabinet that projects downwards onto a mirror.
This produces a pretty good effect that looks like 3d holograms flying around inside the game. 

Unfortinately the CRT monitor in my machine is broken. It turns on, but the only image that appears is a vertical 
green horizontal line in the center of the screen. This is caused by a problem that called "vertical collapse" and to explain what this is, I will need to very quickly 
explain why this happens, we need to quickly explain how a CRT works. 

### How a CRT Works 
A CRT television works by using an electron gun to fire electrons at a phosphor coating on the inside of the screen. 
The gun is initially pointed directly in the middle of the screen, and when the screen is rendered, the gun is 
positioned at the top left of the screen, then moves horizontally to the right, one line at a time. 

Once the end of the screen is reached, the gun resets itself back to the left side of the screen, moves down
by one line and the process repeats.

![starwars-pinball](/assets/images/arcade/starwars-pinball-refurbish/electrongun.png)



*A diagram showing how a CRT works. Source: https://www.chegg.
com/homework-help/questions-and-answers/cathode-ray-tube-crt-used-accelerate-electrons-televisions-computer-monitors-oscilloscopes-q26667986*



*Side note: In the retro video game world, the time period when the gun resets from the right side of the screen 
back to 
the left is called "HBLANK". During this period, you can be sure that the TV is not drawing anything, and thus you can 
safely update video memory without causing any visual glitches. [Mode 7](https://en.wikipedia.org/wiki/Mode_7) 
Graphics on the SNES used this technique to achieve some pretty cool effects, for example.*

When a monitor has vertical collapse the electron gun "gets stuck" and can no longer move vertically, meaning the 
gun moves left to right, but never moves down to the next line. This causes the image to be rendered as a single line
in the middle of the monitor. It usually looks like a green line with some static, but really what you are seeing is 
the entire square image rendered on a single line of the screen.

![starwars-pinball](/assets/images/arcade/starwars-pinball-refurbish/verticalcollapse.jpg)

*An arcade monitor suffering from vertical collapse. Source: https://www.videogamesage.
com/blogs/entry/658-wg-u5000-monitor-vertical-collapse/*

The fix for this issue is different for every CRT monitor. In the case of the Ducksan CGM-1901 chassis in the SWEP1 
pinball, the issue is caused by a faulty IC that is soldered on the circuit board. The repair is pretty 
straightforward, you just buy a replacement IC and replace the old one, but to do so, you need a hot air rework station 
which I do not have. So instead, we are going to replace the CRT monitor with an LCD panel. 

*Side Note: [Hire Me](/contact) so I can afford to buy a hot air rework station and show you how to use it!* 



.
### Converting RGB to VGA
The Starwars episode pinball machine is ran by a semi normal computer that outputs video using a VGA cable. 
However, although it uses a VGA cable, it's actually not outputting a VGA signal. The history of VGA is kind of a 
rabbit hole that deserves it's own post, but the long and short of it is that VGA actually refers to an old protocol 
that just happens to run over a 15 pin DSUB connector. There are other protocols that also use the same cable, but use 
the pins for different purposes than VGA. 

![starwars-pinball](/assets/images/arcade/starwars-pinball-refurbish/starwarscrt.webp)

*A picture of the CRT monitor from the Star Wars Pinball Machine. Notice that it uses a VGA cable so it can connect 
directly to the PC, but this CRT is not "VGA"*

Most arcade machine CRTs from the 80s and 90s (SWEP1 included) use a protocol that is very similar to the [SCART](https://en.wikipedia.org/wiki/SCART)
protocol used in europe, just without the audio part. The video signal consists of 4 wires: Red, Blue, Green, and 
Sync and runs at roughly 15khz. The sync cable combines the horizontal and vertical sync signals into a single wire.

A VGA (technically [SVGA](https://en.wikipedia.org/wiki/Super_VGA)) signal is similar but not the same. It also uses
an RGB type signal, but the sync signal is split into two wires: Horizontal and Vertical. The sync signal is also
a different frequency, running at 31khz (The higher the khz, the higher the resolution).

![starwars-pinball](/assets/images/arcade/starwars-pinball-refurbish/vgapinout.png)

*A diagram showing the pinout of a VGA Connector. Source: https://www.nextpcb.com/blog/vga-connector-pinout*


We need to convert the 15khz signal from the pinball machine to a 31khz signal that the LCD panel can understand. We 
can do this using a [CGA/EGA/YUV to VGA converter board](https://www.aliexpress.com/item/4000042626038.html).

I used an old VGA cable as a donor cable and cut off the end. I then proceeded to solder these wires to the RGB Sync 
header on the converter board. 

![starwars-pinball](/assets/images/arcade/starwars-pinball-refurbish/convertercable.webp)

*A homemade RGB to VGA adapter cable. Notice that most of the wires have been shrink wrapped, but the sync wires 
have just been wraped with electrical tape.*

For most machines that you want to convert to LCD, you can just wire them directly to the board like we have done 
here. However, since the since the star wars pinball uses a mirror to reflect the image onto the playfield, we 
have to take a few extra steps otherwise the image will be backwards.  

To demonstrate this, I went ahead and wired up the SYNC signals directly to the converter board and hooked the cable 
up to the starwars pinball. 




