---
extends: _layouts.blog
section: blog
title: Batch Converting PNG files to WEBP
summary: 'A script I made to convert png files to webp'
order: 8
category: blog
screenshots: 5
date: Fall 2023
tags: png,webp,bash
published: true
---

Recently, I was tasked with taking a large directory of png images and converting them to [webp format](https://developers.google.com/speed/webp)

It turns out Affinity Photo does not support webp, so this ended up being a slightly 
tricky compared to converting other image formats. I'm writing this post to document the process to save myself and
others time in the future.

### Installing CWebP
There is a command line app that can be used to convert images to webp format. This app is called cwebp and can be 
installed by 
[downloading it directly from Google](https://developers.google.com/speed/webp/docs/precompiled)

Or, if you are using a mac, using [Homebrew](https://formulae.brew.sh/formula/webp).

### Running CWebP
You can use cwebp to convert an image like this 

```
cwebp inout.png -o image.webp
```

Where input.png is an image file (other types such as jpg are also supported) and image.webp is the output file.

### Batch Converting A Directory Of Files

I had a directory containing many files that needed to be converted, so I whipped up a simple bash script to convert
an entire directory with a single command. Note that this script expects you to be in the correct directory before 
it runs. 

```bash
for file in ./*; 
do 
  cwebp $file -o `echo $file | sed 's/\.\///g' | sed 's/\.png/\.webp/g'` ; 
done
```

This script runs once for every file in the current directory converting "{ filename }.png" to "{ filename }.webp".
We pipe the name of the file through sed to remove the prefix and extension

./1.png

will become 

./1.webp 


### A Standalone converting function
This BASH function is also pretty useful. It assumes that you are in the directory with the image and that the image
you are converting is a png, but you could easily modify it to suit your needs.

```bash
convert () { cwebp $1.png -o $1.webp; }
```

Assuming you had an image called hello.png, You could use the method like this

```bash
cd /path/to/image 
convert hello
```

Afterwards there will now be a new "hello.webp" file in the directory

Very handy! Hope this helps someone else out there too. 

