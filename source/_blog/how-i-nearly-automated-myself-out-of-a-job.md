---
extends: _layouts.blog
section: blog
title: How I nearly accidentally automated my entire department out of a job
featuredimg: '/assets/images/septicdark.png'  
summary: 'A story of an internship I did with a governmenmental IT Dept'
order: 2
category: blog
screenshots: 5
date: Spring 2017
tags: story
published: true
---

During the summer of my second year of University, I managed to snag myself an internship position in the IT Department of my Provincial Government. The job was part of a country wide initiative to reduce governmental e-waste by taking computers removed from government departments (replaced with newer models) then refurbishing and re-purposing these computers for use in schools, libraries, and other non-profit community initiatives. 

The Job consisted of two distinct responsibilities:

- On even days, we would go on site to various government departments (schools, office buildings, etc) with a truck full of brand new computers that we would install on site. The old machines were loaded on to the truck and hauled back to be refurbished at IT headquarters.

- On odd days we would spend the day fixing/refurbishing these machines to make computers that could be reused in schools, libraries, and other non-profit type locations.

  

  The job was pretty tedious and could essentially be broken down into the following steps

  - Walk to the warehouse, take a computer off the rack, and carry it back to your workstation. The warehouse was quite far from the workstations, averaging around five minutes round trip per machine.
  - Clean the outside of the case, then open it up and blow the dust out of the machine. 
  - If the machine is a "Dell", we would check the capacitors to ensure none of them are faulty (Side note, did you know Dell computers from the early 2000's are estimated to have up to [97% failure rates](https://www.google.ca/amp/s/amp.hothardware.com/news/dell-blames-97-optiplex-failure-rate-on-everyone-else)?)

  - Turn it on to confirm it works 
  - Write the specs of the machine (graphics, ram, hard drive, cpu) on a standardized form. This form was then taped to the side of the case and the machine was placed in the "Ready for non-profits" pile.  

- - Repeat until the end of the summer. 



### Passing the time through increased productivity

The job was a government position so it was incredibly slack. There were no real deadlines, you just worked at your own pace and whatever you got done by the end of the summer was good enough.  This was fine for some of the interns, but for myself and another intern, Aaron who were used to hard work, this environment became really boring. We started trying to find ways to automate things and increase productivity as a way to pass the time. 

- First we found a cart with wheels that we could take with us to the warehouse and carry a bunch of computers back at once.  Walking back and forth from the warehouse was by far the most time consuming part of the job, so our cart was a pretty significant speedup. 

- Next, we took a look at how we could optimize our workstations. Each station had enough space, power outlets, and ethernet jacks for each tech to work on multiple machines at once, but for whatever reason, each workstation only had one working ethernet port. With the help of some network techs, we managed to get all of the non working ethernet ports hooked up to real ports on a switch in the server room. Now each of us could diagnose 2-3 computers at once. 

- Finally, instead of each person completing the entire job at once, we started working together, creating an assembly line of computer refurbishment. One person would clean the pcs and set them up at each diagnostic station, the other would start them up, confirm they worked, and collected the specs for each machine. 

Our assembly line was very effective and by the end of the summer, we had refurbished and delivered many multiples more machines then the techs who chose to go it on their own. At the end of the summer I went back to school to complete the third year of my degree. 



### Cranking up the automation to 11

The next summer, I was again offered an intern position with my provincial government IT department. The job was exactly the same as the previous summer except for one thing: my assembly line partner, Aaron was no longer an intern. The hard work we did the previous summer had not gone unnoticed and he had been offered a well deserved full time position within the department. He was replaced by a team of kids from a local trade school that had no interest in hard work. My assembly line was now a line of one and I was on my own to find new ways to automate. 

One of the courses I had taken during the school year was about system level programming in Linux. The Linux command terminal is very powerful, Anything you could ever want to access within your computer is always a few keystrokes away. This includes not just access to all hardware and software, but also access to anything in memory, swap partitions, or even bios settings. You can also chain various commands together, using the output of one command as the input into the next, meaning the only limiting factor was your own creativity.

I had an idea that I could write a custom linux based OS that on bootup would gather all of the information that would previously need to be found manually. Since the job was very much a "work at your own pace" position, for the next few weeks I started taking part of my day to further research this idea. To give you a better idea of how slack this job was, even with the extra downtime I was taking, I still managed to get more work done than some of the other interns. 

In the end, I had come up with a clever script that used 

- ***lscpu*** to retrieve the type of processor installed in the system

-  ***lshw***  to gather information about RAM, Hard Drives, USB ports
- ***dmidecode*** to gether the manufacturer information (DELL, HP, etc), model numbers, serial numbers, and other "system specific" information that was unique to each machine 
- ***awk*** and ***sed*** to format all of this data into a text document that was more or less the same as what was previously filled out by hand
- The system would then connect to a network connected printer and automatically print this document.

I loaded this script on to a custom Linux live CD that would boot directly from the CD drive. As soon as you powered on the machine, the script would immediately execute. All that was required was to hook up each machine to power and ethernet, turn it on, insert the CD (There was no GUI, so you didn't even need to hook up a monitor). A few seconds later the information would be printed off and could be taped to the side of the machine. 

You could determine if there was any errors by counting the number of pages printed. If the number of pages printed was less than the number of machines you were working on, then one of the machines was defective. To determine which machine had the problem you simply matched each machine to its spec sheet using the serial number. If a machine didn't have a matching piece of paper, it was defective and could be thrown in the defective pile. 



### Demonstrating my work to my supervisors

I estimated that I could now do more work in a single week then our entire team was expected to complete in an entire summer.  I excitedly brought this to my supervisior with a demonstration of one tech refurbishing 12 computers in under a minute.  My supervisor was blown away, but not in a good way. I was told there was no way they could implement this system *because it was way too efficient*. If they started using this they wouldn't have any more work for the interns to do and (his words) would have to lay everyone off. 

I suddenly understood what people meant when they talked about how lazy and innefficient government workers were. I could have pursued things further, and showed my work off to higher ups, but I realized there was no point. A career path where innovation is stifled was not something I was going to be happy working at anyways. I decided then and there against a career in government, and looking back it was definitely the correct choice to make.
