@extends('_layouts.main')

@section('body')
    <div class="">
        <div class="px-4 py-5">
            <div class="container-fluid">
                <div class="p-5 mb-5 bg-light">
                    <div class="row">
                        <div class="col-sm-3"><img class="img-fluid img-thumbnail rounded-circle" src="/assets/theme/img/andrew2.jpg"
                                                   alt=""></div>
                        <div class="col-sm-9">
                            <h1 class="font-weight-normal">Hello! I'm <strong>Andrew Reddin</strong>. </h1>
                            <h2 class="h1 font-weight-normal mb-4">I'm a developer with a passion for building innovative
                                projects.
                            </h2>
                            <p class="text-muted mb-4">I’m a software developer with over 10 years of experience. These days, I mostly
                                assist with building and maintaining Laravel, Symfony, and Vue.js applications.</p>

                            <p class="text-muted mb-4">In my spare time I collect and restore vintage arcade & pinball
                                machines from the 80's and 90's. I have extensive knowledge of the inner workings of
                                electronics that are a fraction as powerful as my cell phone.</p>

                        </div>
                    </div>
                </div>
                <h2 class="h1">Work experience</h2>
                <p class="text-muted mb-4">Here are some of the things I can help you with</p>
                <div class="row mb-4">
                    <div class="col-lg-6 mb-4">
                        <div class="d-flex">
                            <div class="flex-grow-1" style="color: #EF5285;">
                                <svg class='svg-icon text-primary' xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64">
                                    <rect data-name="layer2" x="2" y="14" width="60" height="48" rx="2" ry="2" fill="none"
                                          stroke-linecap="round" stroke-linejoin="round"
                                          stroke-width="3"></rect>
                                    <path data-name="layer2"
                                          d="M2.4 14.8L7.4 4A3 3 0 0 1 10 2h44a2.9 2.9 0 0 1 2.5 2l5.2 10.8" fill="none"
                                          stroke-linecap="round" stroke-linejoin="round"
                                          stroke-width="3"></path>
                                    <path data-name="layer1" fill="none" stroke-linecap="round"
                                          stroke-linejoin="round" stroke-width="3" d="M54 54H34"></path>
                                    <path data-name="layer2" fill="none" stroke-linecap="round"
                                          stroke-linejoin="round" stroke-width="3" d="M2 46h60"></path>
                                    <path data-name="layer1" fill="none" stroke-linecap="round"
                                          stroke-linejoin="round" stroke-width="3" d="M54 38H34m20-16H34"></path>
                                    <path data-name="layer2" fill="none" stroke-linecap="round"
                                          stroke-linejoin="round" stroke-width="3" d="M2 30h60"></path>
                                    <circle data-name="layer1" cx="11" cy="22" r="2" fill="none"
                                            stroke-linecap="round" stroke-linejoin="round" stroke-width="3"></circle>
                                    <circle data-name="layer1" cx="11" cy="38" r="2" fill="none"
                                            stroke-linecap="round" stroke-linejoin="round" stroke-width="3"></circle>
                                    <circle data-name="layer1" cx="11" cy="54" r="2" fill="none"
                                            stroke-linecap="round" stroke-linejoin="round" stroke-width="3"></circle>
                                </svg>

                            </div>
                            <div class="ml-3">
                                <h3 class="h5 mr-3">Back End Development</h3>
                                <p class="text-muted mb-2">My back-end language of choice is PHP paired with Laravel, but
                                    i've also worked with Symfony, Ruby On Rails, and .NET</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 mb-4">
                        <div class="d-flex">
                            <div class="flex-grow-1">
                                <svg class="svg-icon text-primary">
                                    <use xlink:href="#multiple-windows-1"></use>
                                </svg>
                            </div>
                            <div class="ml-3">
                                <h3 class="h5 mr-3">Front End Development</h3>
                                <p class="text-muted mb-2">My front-end framework of choice is Vue.js with Vuex for state
                                    management. I've also worked with Angular, Backbone, Ext.js, Ember, and JQuery.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 mb-4">
                        <div class="d-flex">
                            <div class="flex-grow-1">
                                <svg class="svg-icon text-primary">
                                    <use xlink:href="#design-1"></use>
                                </svg>
                            </div>
                            <div class="ml-3">
                                <h3 class="h5 mr-3">Project Management</h3>
                                <p class="text-muted mb-2">I have experience with agile project management both as scrum
                                    master and as a cog in the development machine. </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 mb-4">
                        <div class="d-flex">
                            <div class="flex-grow-1">
                                <svg class="svg-icon text-primary" style="color: #EF5285;">
                                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64">
                                        <circle data-name="layer2" cx="32" cy="39" r="7" fill="none"
                                                stroke-miterlimit="10" stroke-width="3" stroke-linejoin="round"
                                                stroke-linecap="round"></circle>
                                        <path data-name="layer2"
                                              d="M32 46a12.1 12.1 0 0 0-12 12v2h24v-2a12.1 12.1 0 0 0-12-12z" fill="none"
                                              stroke-miterlimit="10" stroke-width="3"
                                              stroke-linejoin="round" stroke-linecap="round"></path>
                                        <circle data-name="layer2" cx="52" cy="10" r="6" fill="none"
                                                stroke-miterlimit="10" stroke-width="3" stroke-linejoin="round"
                                                stroke-linecap="round"></circle>
                                        <path data-name="layer2" d="M62 28c0-7.5-4.5-12-10-12s-10 4.5-10 12z" fill="none"
                                              stroke-miterlimit="10" stroke-width="3"
                                              stroke-linejoin="round" stroke-linecap="round"></path>
                                        <circle data-name="layer2" cx="12" cy="10" r="6" fill="none"
                                                stroke-miterlimit="10" stroke-width="3" stroke-linejoin="round"
                                                stroke-linecap="round"></circle>
                                        <path data-name="layer2" d="M22 28c0-7.5-4.5-12-10-12S2 20.5 2 28z" fill="none"
                                        " stroke-miterlimit="10" stroke-width="3"
                                        stroke-linejoin="round" stroke-linecap="round"></path>
                                        <path data-name="layer1" fill="none" stroke-miterlimit="10"
                                              stroke-width="3" d="M12 34l8 8m32-8l-8 8M24 14h16" stroke-linejoin="round"
                                              stroke-linecap="round"></path>
                                    </svg>
                                </svg>
                            </div>
                            <div class="ml-3">
                                <h3 class="h5 mr-3">Team Leadership</h3>
                                <p class="text-muted mb-2">I've been in charge of teams as large as 5 people (not including
                                    myself) and have managed both junior and senior developers.</p>
                            </div>
                        </div>
                    </div>
                </div>

                <!--
                <h2 class="h1">Testimonials</h2>
                <p class="text-muted mb-4">Received overcame oh sensible so at an. Formed do change merely to county it.</p>
                <div class="row text-center align-items-stretch">
                    <div class="col-lg-3 mb-4">
                        <div class="border p-4 h-100 d-flex align-items-center">
                            <div class="w-100">
                                <svg class="svg-icon text-gray mb-2">
                                    <use xlink:href="#stack-1"></use>
                                </svg>
                                <h5>Happy Clients</h5>
                                <p class="mb-0 text-gray h5">1200</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 mb-4">
                        <div class="border p-4 h-100 d-flex align-items-center bg-light">
                            <div class="w-100">
                                <svg class="svg-icon text-gray mb-2">
                                    <use xlink:href="#quality-1"></use>
                                </svg>
                                <h5>Awards Won</h5>
                                <p class="mb-0 text-gray h5">18</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 mb-4">
                        <div class="border p-4 h-100 d-flex align-items-center">
                            <div class="w-100">
                                <svg class="svg-icon text-gray mb-2">
                                    <use xlink:href="#time-1"></use>
                                </svg>
                                <h5>Working Hours</h5>
                                <p class="mb-0 text-gray h5">6500</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 mb-4">
                        <div class="border p-4 h-100 d-flex align-items-center bg-light">
                            <div class="w-100">
                                <svg class="svg-icon text-gray mb-2">
                                    <use xlink:href="#hot-coffee-1"></use>
                                </svg>
                                <h5>Coffee Consumed</h5>
                                <p class="mb-0 text-gray h5">18000</p>
                            </div>
                        </div>
                    </div>
                </div>
                !-->
            </div>
        </div>

    </div>
@endsection
